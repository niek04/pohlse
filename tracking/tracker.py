import pickle
import tkinter as tk
from pathlib import Path
from tkinter import filedialog
from moviepy.editor import VideoFileClip

import cv2
import numpy as np

# Initialise directories
video_folder_name = "videos"
video_folder_path = Path(__file__).parent.joinpath(video_folder_name).resolve()
videos = tk.filedialog.askopenfilenames(initialdir=video_folder_path)

# Limits to find colors, bgr format, pm 20px

# Limits for blue sticker
upperlimit_blue = np.array([180, 160, 130])
lowerlimit_blue = np.array([130, 110, 70])

# Limits for red sticker
upperlimit_red = np.array([40, 80, 255])
lowerlimit_red = np.array([0, 40, 220])

# Limits for green sticker
upperlimit_green = np.array([135, 167, 51])
lowerlimit_green = np.array([95, 127, 11])

min_area = 20
playspeed = 1

def track(src, min_area = 20, playspeed = 1):
    count = 0
    vid = cv2.VideoCapture(src)
    clip = VideoFileClip(src)
    duration = clip.duration

    x_cords_blue, y_cords_blue, x_cords_red, y_cords_red, x_cords_green, y_cords_green, framecount = [], [], [], [], [], [], []

    def entered_escape():
        return cv2.waitKey(playspeed) == 27

    while not entered_escape():
        frame = vid.read()[1]
        if frame is None:
            break

    
        bluecheck = 0
        redcheck = 0
        greencheck = 0

        count += 1
        framecount.append(count)

        # Object detection
        clrmask_blue = cv2.inRange(frame, lowerlimit_blue,
                                   upperlimit_blue)  # Create a mask of only pixels within limits
        contours_blue, _ = cv2.findContours(clrmask_blue, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        clrmask_red = cv2.inRange(frame, lowerlimit_red, upperlimit_red)
        contours_red, _ = cv2.findContours(clrmask_red, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        clrmask_green = cv2.inRange(frame, lowerlimit_green, upperlimit_green)
        contours_green, _ = cv2.findContours(clrmask_green, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # Object tracking
        for i in contours_blue:
            area_blue = cv2.contourArea(i)

            # Only draw contour if area is bigger then 100px to remove noise
            if area_blue > min_area and bluecheck == 0:
                x_blue, y_blue, w_blue, h_blue = cv2.boundingRect(i)
                cv2.rectangle(frame, (x_blue, y_blue), (x_blue + w_blue, y_blue + h_blue), (0, 255, 0), 2)
                # coordinates_blue.append([x_blue,y_blue,count])
                x_cords_blue.append([x_blue])
                y_cords_blue.append([y_blue])
                bluecheck = 1

        for i in contours_red:
            area_red = cv2.contourArea(i)

            # Only draw contour if area is bigger then 100px to remove noise
            if area_red > min_area and redcheck == 0:
                x_red, y_red, w_red, h_red = cv2.boundingRect(i)
                cv2.rectangle(frame, (x_red, y_red), (x_red + w_red, y_red + h_red), (0, 255, 0), 2)
                x_cords_red.append([x_red])
                y_cords_red.append([y_red])
                redcheck = 1

        for i in contours_green:
            area_green = cv2.contourArea(i)

            # Only draw contour if area is bigger then 100px to remove noise
            if area_green > min_area and greencheck == 0:
                x_green, y_green, w_green, h_green = cv2.boundingRect(i)
                cv2.rectangle(frame, (x_green, y_green), (x_green + w_green, y_green + h_green), (0, 255, 0), 2)
                x_cords_green.append([x_green])
                y_cords_green.append([y_green])
                greencheck = 1

        if redcheck == 0:
            x_cords_red.append([0])
            y_cords_red.append([0])

        if bluecheck == 0:
            x_cords_blue.append([0])
            y_cords_blue.append([0])

        if greencheck == 0:
            x_cords_green.append([0])
            y_cords_green.append([0])

        # Show the frame/masks
        cv2.imshow("Main", frame)
        cv2.imshow("Color mask blue", clrmask_blue)
        cv2.imshow("Color mask red", clrmask_red)
        cv2.imshow("Color mask green", clrmask_green)

    vid.release()
    cv2.destroyAllWindows()

    print('Length of framecount: ', len(framecount), ' Length of green x coords: ', len(x_cords_green), ' Length of red x coords: ', len(x_cords_red),' Length of blue x coords: ', len(x_cords_blue),)

    return x_cords_blue, y_cords_blue, x_cords_red, y_cords_red, x_cords_green, y_cords_green, framecount, duration


# Run for each video

for video in videos:
    filename = Path(video).stem
    results = track(video, min_area, playspeed)
    _, _, _, _, _, _, framecount, _ = results

    last_frame = framecount[len(framecount) - 1]
    plk_name = f"{filename}_upto_{last_frame}.plk"
    plk_path = Path(__file__).parent.joinpath("results").joinpath(plk_name).resolve()

    with open(plk_path, 'wb') as file:
        pickle.dump(results, file)

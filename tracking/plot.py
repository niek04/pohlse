import pickle
import statistics as st
import tkinter as tk
from pathlib import Path
from tkinter import filedialog
from matplotlib.pyplot import figure
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
import numpy as np
import locale
import scipy as sp
import pandas as pd
import scipy


locale.setlocale(locale.LC_NUMERIC, "de_DE")

period_by_video = []
period_motor_by_video = []
amplitude = []
finale_periods = []
number_peaks = []
const = 1

def process_data(x_cords_blue, y_cords_blue, x_cords_red, y_cords_red, x_cords_green, y_cords_green):
    # Need to resort all arrays into single level array
    x_blue = [item for sublist in x_cords_blue for item in sublist]
    y_blue = [item for sublist in y_cords_blue for item in sublist]
    x_green = [item for sublist in x_cords_green for item in sublist]
    y_green = [item for sublist in y_cords_green for item in sublist]
    x_red = [item for sublist in x_cords_red for item in sublist]
    y_red = [item for sublist in y_cords_red for item in sublist]

    avg_x = st.mean(x_red)
    avg_y = st.mean(y_red)

    comp_x = list(map(lambda e: e - avg_x, x_blue))
    comp_y = list(map(lambda e: (e - avg_y) * -1, y_blue))
    radius = list(map(lambda e: (e[0]**2 + e[1]**2) ** 0.5, zip(comp_x, comp_y)))
    # print('radius: ', radius, ' len: ', len(radius))
    angle = []
    for i in range(len(radius)):
        if comp_y[i] >= 0:
            angle.append(np.arctan(comp_x[i] / comp_y[i])*(180/np.pi))
        elif comp_y[i] <= 0 and comp_x[i] <= 0:
            angle.append(-np.arctan(comp_y[i] / comp_x[i])*(180/np.pi) - 90)
        elif comp_y[i] <= 0 and comp_x[i] >= 0:
            angle.append(-np.arctan(comp_y[i] / comp_x[i])*(180/np.pi) + 90)

    # print("Len time, len angle: ", len(time), ', ', len(angle))

    return comp_x, comp_y, radius, angle


path = Path(__file__).parent.joinpath("results").resolve()
srcList = tk.filedialog.askopenfilenames(initialdir=path)

for src in srcList:

    with open(src, 'rb') as b:
        results = pickle.load(b)

    x_cords_blue, y_cords_blue, x_cords_red, y_cords_red, x_cords_green, y_cords_green, framecount, duration = results

    time = []
    for i in range(len(framecount)):
        time.append(i*duration/len(framecount))

    comp_x, comp_y, radius, angle = process_data(x_cords_blue, y_cords_blue, x_cords_red, y_cords_red, x_cords_green,
                                                 y_cords_green)





    # find peaks in the angle as function of time graph for the pendulum
    peaks_x = find_peaks(angle, height=0, distance = 50)[0]
    peaks_xcor = list(map(lambda e: e*duration/len(framecount), peaks_x))

    peaks_ycor = list(map(lambda e: angle[e], peaks_x))
    periods = []
    for i in range(len(peaks_xcor)-1):
        periods.append(peaks_xcor[i+1]-peaks_xcor[i])
        # print("test")
    period = st.mean(periods)

    # plots a histogram of the calculated radii
    def gauss(mean, std, x):
        fit = 1/(std*(2*np.pi)**0.5)*np.exp(-0.5*((x-mean)/std)**2)
        return fit

    def histo():
        plt.rcParams['axes.formatter.use_locale'] = True
        radius_cm = list(map(lambda e: e*8/st.mean(radius), radius))
        gaussianx = np.arange(min(radius_cm),max(radius_cm),0.01)
        gaussian_fit = list(map(lambda e: gauss(st.mean(radius_cm), st.stdev(radius_cm), e), gaussianx))
        plt.plot(gaussianx,gaussian_fit, color = 'black')
        plt.ylabel('Kansdichtheid', fontsize=12)
        plt.xlabel('Radius R[cm]', fontsize=12)
        plt.hist(radius_cm, density = True, bins= round(0.5*len(radius_cm)**0.5), color = 'gray')
        textstr = '\n'.join((
            r'$\mu=%.2f$ cm' % (st.mean(radius_cm), ),
            r'$\sigma=%.2f$ cm' % (st.stdev(radius_cm), )))
        props = dict(boxstyle='round', facecolor='azure', alpha=0.5)
        plt.text(7.82, 16, textstr, fontsize=14,
                verticalalignment='top', bbox=props)
        plt.xlim(7.8, 8.2)
        plt.grid(axis = 'y')
        plt.savefig('hist.pdf')
        plt.show()





    # plots the angel as function of time
    def plot():
        figure(figsize=(10, 5), dpi=80)
        plt.plot(time, angle, color = 'coral', label = 'Verloop van de Pohlse slinger')
        plt.scatter(peaks_xcor, peaks_ycor, zorder=10, label = 'Pieken')
        plt.ylabel('Hoek $φ[\degree]$ ', fontsize=12)
        plt.xlabel('Tijd t[s]', fontsize=12)
        # plt.title(src)
        plt.legend()
        plt.grid()
        plt.xlim(min(time), max(time))
        plt.savefig(f'no_motor_no_mass{const}.pdf')
        plt.show()
    const += 1

    # creates x-list and y-list for the motor.
    x_as = []
    y_as = []
    p = 0
    for i in y_cords_green:
        if i[0] >= 400:
            x_as.append(time[p])
            y_as.append(i)
        p += 1

    # finds the frequenty of the motor
    y_as_motor = list(map(lambda e: e[0], y_as))
    peaks_x_motor = find_peaks(y_as_motor, height=1000, distance = 1)[0]
    peaks_xcor_motor = list(map(lambda e: e*duration/len(framecount), peaks_x_motor))

    peaks_ycor_motor = list(map(lambda e: y_as[e], peaks_x_motor))
    periods_motor = []
    for i in range(len(peaks_xcor)-1):
        periods_motor.append(peaks_xcor_motor[i+1]-peaks_xcor_motor[i])
    period_motor = st.mean(periods_motor)
    sample_rate = len(framecount)/duration

    N = round(sample_rate * duration)
    yf = sp.fft.fft(y_as_motor)
    xf = sp.fft.fftfreq(len(yf), 1 / sample_rate)
    xf_plot = []
    yf_plot = []
    for i in range(len(np.abs(yf))):
        if np.abs(yf)[i] <= 2e6:
            xf_plot.append(xf[i])
            yf_plot.append(np.abs(yf)[i])
    def freq_plot():
    # plot in Tijddomein
        plt.ylabel('Hoek $φ[\degree]$ ', fontsize=12)
        plt.xlabel('Tijd t[s]', fontsize=12)
        plt.plot(x_as, y_as, color = "green")
        plt.grid()
        plt.show()
        plt.xlim(0,2.5)

        # plot in Frequentiedomein
        # plt.yticks([])
        plt.xlabel('Frequentie f[Hz]', fontsize=12)
        plt.plot(xf_plot, np.abs(yf_plot),  color = "red")
        plt.grid()
        plt.show()




    # Find the maximum y value
    max_y = max(yf_plot)
    # Find the x value corresponding to the maximum y value
    max_freq = xf_plot[yf_plot.index(max_y)]
    period_motor_by_video.append(max_freq * 2 * np.pi)
    # add the end amplitude per video
    amplitude.append(max(peaks_ycor[round(0.95 * len(peaks_ycor)): len(peaks_ycor)]))
    # add the period per video
    period_by_video.append(period)




    #__________________________________________________________________________________________________________________

    print(comp_y)
    # finds the frequenty of the end oscillation


    N = round(sample_rate * duration)
    yf = sp.fft.fft(angle[round(0.8 * len(angle)): len(angle)])
    xf = sp.fft.fftfreq(len(yf), 1 / sample_rate)
    xf_plot = []
    yf_plot = []
    for i in range(len(np.abs(yf))):
        if np.abs(yf)[i] <= 2e6:
            xf_plot.append(xf[i])
            yf_plot.append(np.abs(yf)[i])


    def freq_end_plot():
        # plot in Tijddomein
        plt.ylabel('Hoek $φ[\degree]$ ', fontsize=12)
        plt.xlabel('Tijd t[s]', fontsize=12)
        plt.plot(time, angle, color="green")
        plt.grid()
        plt.show()

        # plot in Frequentiedomein
        # plt.yticks([])
        plt.xlabel('Frequentie f[Hz]', fontsize=12)
        plt.stem(xf_plot, np.abs(yf_plot), markerfmt=" ")
        plt.grid()
        plt.xlim(0, 0.5)
        plt.show()



    # Find the maximum y value
    max_y = max(yf_plot[round(0.1*len(yf_plot)): len(yf_plot)])
    # Find the x value corresponding to the maximum y value
    max_freq = xf_plot[yf_plot.index(max_y)]
    finale_periods.append(max_freq * 2 * np.pi)
    # add the period per video
    period_by_video.append(period)




    #__________________________________________________________________________________________________________________




max_y = max(amplitude)  # Find the maximum y value
max_freq = period_motor_by_video[amplitude.index(max_y)]  # Find the x value corresponding to the maximum y value


def rest_freq():
    plt.xlabel('Hoekfrequentie Motor $\omega[rad/s]$', fontsize=12)
    plt.ylabel('Eind amplitude Slinger $φ[\degree]$', fontsize=12)
    plt.scatter(period_motor_by_video, amplitude, color = 'steelblue')
    plt.vlines(max_freq, 0, 150, color = 'black', linestyles='--', label = '$\omega_n = 2{,}8 $ rad/s ')
    print(max_freq)
    line1 = max_freq + (period_motor_by_video[9] - period_motor_by_video[10])
    line2 = max_freq + (period_motor_by_video[11] - period_motor_by_video[10])
    plt.axvspan(line1, line2, alpha=.2, color='green', label = '$\Delta \omega_n = 0{,}4$  rad/s')
    plt.ylim(0, 120)
    plt.legend()
    plt.grid()
    plt.savefig('res.pdf')
    plt.show()

def end_freq():
    plt.xlabel('Hoekfrequentie Motor $\omega[rad/s]$', fontsize=12)
    plt.ylabel('maximum eind periode $φ[\degree]$', fontsize=12)
    plt.scatter(period_motor_by_video, finale_periods, color = 'steelblue')
    # plt.ylim(0, 120)
    plt.legend()
    plt.grid()
    plt.savefig('res.pdf')
    plt.show()
end_freq()
